from matplotlib.pyplot import scatter, show, ylabel, xlabel


def main():
    with open("temp") as temp:
        lines = [s.split(' ') for s in temp.readlines()]

        time = [int(s[0]) for s in lines]
        mes = [int(s[1]) for s in lines]

        scatter(time, mes)
        ylabel('Temperature')
        xlabel('Time')
        show()


if __name__ == "__main__":
    main()
