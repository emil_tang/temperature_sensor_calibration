from serial import Serial
from time import perf_counter_ns

DEVICE: str = '/dev/ttyACM0'
B_RATE: int = 115200


def main():
    with open("temp", "a+") as temp, Serial(DEVICE, B_RATE, timeout=0) as port:
        while True:
            if port.in_waiting > 0:
                bytes = port.readline()
                string = str(bytes, 'utf-8').strip()
                temp.write(f'{perf_counter_ns()} {string}\n')
                print(string)


if __name__ == '__main__':
    main()
