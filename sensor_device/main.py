from time import sleep
from machine import Pin
from machine import ADC

adc = ADC()

temp_sensor = adc.channel(pin='P16')
print(adc.vref())
p = Pin('P19', Pin.OUT, 1)

while True:
    val = temp_sensor.value()
    print(val)
    sleep(1)
